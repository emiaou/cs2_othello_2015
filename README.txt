Last week, I only checked some of the spots adjacent to corners and subtracted
points in my heuristics if a piece was placed in those areas. This discourages the AI from enabling
the opponent to place a corner piece, which would take a lot of pieces. I was able to beat only 
SimplePlayer with this heuristic. 
This time, I also checked the spots diagonal to the corners and subtracted an equal
number of points if game pieces were placed in those spots. Simply adding this additional
condition allowed me to beat ConstantTimePlayer almost all the time (as both white or black player).