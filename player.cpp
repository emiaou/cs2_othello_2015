#include "player.h"
#include <vector>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) 
{
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     mySide = side;
     if(mySide == BLACK)
     {
         opponentsSide = WHITE;
     }
     else
     {
         opponentsSide = BLACK;
     }
}

/*
 * Destructor for the player.
 */
Player::~Player() 
{
    
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    
    /* 
     * The simplest, most basic working AI
    board.doMove(opponentsMove, opponentsSide);
    Move *m;
    if(board.hasMoves(mySide))
    {
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                m = new Move(i, j);
                if(board.checkMove(m, mySide))
                {
                    board.doMove(m, mySide);
                    return m;
                }
            }
        }
    }
    return NULL;
    */
    vector<Move *> allMoves;
    board.doMove(opponentsMove, opponentsSide);
    Move *m;
    int highestScore = -64;
    Move *highestMove;
    if(board.hasMoves(mySide))
    {
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                m = new Move(i, j);
                if(board.checkMove(m, mySide))
                {
                    allMoves.push_back(m);
                }
            }
        }
    }
    else
    {
        return NULL;
    }
    for(int i = 0; i < allMoves.size(); i++)
    {
        Board *boardCopy = board.copy();
        boardCopy->doMove(allMoves[i], mySide);
        int temp = boardCopy->count(mySide) - boardCopy->count(opponentsSide);
        
        if((allMoves[i]->x == 0 || allMoves[i]->x == 7) && (allMoves[i]->y == 0 || allMoves[i]->y == 7))
        {
            temp += 10;
        }
        else if((allMoves[i]->x == 0 || allMoves[i]->x == 7) && (allMoves[i]->y == 1 || allMoves[i]->y == 6))
        {
            temp -= 12;
        }
        else if((allMoves[i]->x == 1 || allMoves[i]->x == 6) && (allMoves[i]->y == 0 || allMoves[i]->y == 7))
        {
            temp -= 12;
        }
        else if((allMoves[i]->x == 1 || allMoves[i]->x == 6) && (allMoves[i]->y == 1 || allMoves[i]->y == 6))
        {
            temp -= 12;
        }
        else if(allMoves[i]->x == 0 || allMoves[i]->x == 7 || allMoves[i]->y == 0 || allMoves[i]->y == 7)
        {
            temp += 4;
        }
        if(highestScore < temp)
        {
            highestMove = allMoves[i];
            highestScore = temp;
        }
    }
    board.doMove(highestMove, mySide);
    return highestMove;
}
